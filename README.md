# Asterisk in Docker

## Operations

### Restarting the container

The containers should be launched using a `docker-compose` project which will require a `docker-compose.yml` file in
the `/opt/asterisk` directory.  An example configuration file is provided below in the deployment section.

```bash
cd /opt/asterisk
docker-compose down
docker-compose up -d --force-recreate
```

```bash
docker run \
  --rm \
  --name asterisk \
  --network host \
  --cap-add=SYS_NICE \
  -v /etc/asterisk:/etc/asterisk \
  -v /etc/odbc.ini:/etc/odbc.ini \
  -v /opt/asterisk/bin:/opt/asterisk/bin \
  -v /opt/asterisk/etc:/opt/asterisk/etc \
  -v /opt/asterisk/tmp:/tmp \
  -v /var/lib/asterisk/astdb.sqlite3:/var/lib/asterisk/astdb.sqlite3 \
  -v /var/lib/asterisk/moh:/var/lib/asterisk/moh \
  -v /var/lib/asterisk/sounds:/var/lib/asterisk/sounds \
  -v /var/log/asterisk:/var/log/asterisk \
  -v /var/spool/asterisk:/var/spool/asterisk \
  registry.github.com/joshelson/asterisk-docker:${tag}
```

### Running a shell inside the container

The `media-shell` script starts a `bash` shell inside the container as the user `asterisk`.  This should be suitable
for all media server operations.

```bash
media-shell
asterisk -rvvv
```

If root access is needed within the container, it may be started manually.

```bash
docker exec -it asterisk /bin/bash
```

## Deployment

### Example docker-compose.yml

Replace `${tag}` with the current image tag name and create this file in `/opt/asterisk`.

```yaml
version: '2'
services:
  asterisk:
    image: registry.github.com/joshelson/asterisk-docker:${tag}
    container_name: asterisk
    volumes:
    - /etc/asterisk:/etc/asterisk
    - /etc/odbc.ini:/etc/odbc.ini
    - /opt/asterisk/bin:/opt/asterisk/bin
    - /opt/asterisk/etc:/opt/asterisk/etc
    - /opt/asterisk/tmp:/tmp
    - /var/lib/asterisk/astdb.sqlite3:/var/lib/asterisk/astdb.sqlite3
    - /var/lib/asterisk/moh:/var/lib/asterisk/moh
    - /var/lib/asterisk/sounds:/var/lib/asterisk/sounds
    - /var/log/asterisk:/var/log/asterisk
    - /var/spool/asterisk:/var/spool/asterisk
    cap_add:
    - SYS_NICE
    restart: always
    network_mode: host
```

## Build

## Local Environment

### Building and running an image

```bash
docker build -t joshelson/asterisk-docker:17.7.0 .
docker run \
  --rm \
  --name asterisk \
  --network host \
  --cap-add=SYS_NICE \
  -v /etc/asterisk:/etc/asterisk \
  -v /opt/asterisk/bin:/opt/asterisk/bin \
  -v /opt/asterisk/etc:/opt/asterisk/etc \
  -v /opt/asterisk/tmp:/tmp \
  -v /var/lib/asterisk/moh:/var/lib/asterisk/moh \
  -v /var/log/asterisk:/var/log/asterisk \
  -v /var/spool/asterisk:/var/spool/asterisk \
  joshelson/asterisk-docker:17.7.0
```

### Pushing Container to Dockerhub

```bash
docker push joshelson/asterisk-docker:17.7.0
```
