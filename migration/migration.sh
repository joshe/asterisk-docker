#!/bin/bash

sudo yum -y erase nginx
sudo yum -y update

yum install -y yum-utils epel-release

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

yum -y install docker-ce docker-ce-cli containerd.io docker-compose

sudo systemctl start docker
sudo systemctl enable docker

mkdir -p /opt/asterisk/bin
mkdir -p /opt/asterisk/tmp
mkdir -p /opt/asterisk/etc/nginx/conf.d

cat > /opt/asterisk/docker-compose.yml << EOF
version: '2'
services:
  asterisk:
    image: asterisk-docker:17.7.0
    container_name: asterisk
    user: asterisk:asterisk
    volumes:
    - /etc/asterisk:/etc/asterisk
    - /opt/asterisk/bin:/opt/asterisk/bin
    - /opt/asterisk/etc:/opt/asterisk/etc
    - /var/lib/asterisk/moh:/var/lib/asterisk/moh
    - /var/log/asterisk:/var/log/asterisk
    - /var/spool/asterisk:/var/spool/asterisk
    - /tmp:/tmp
    restart: always
    network_mode: host
    hostname: ${HOSTNAME}

  nginx:
    image: alpine-nginx-asterisk:1.19.1-alpine
    container_name: nginx
    user: asterisk:asterisk
    volumes:
    - /var/log/nginx:/var/log/nginx
    - /etc/asterisk:/etc/asterisk
    - /tmp:/tmp
    - ./etc/nginx/conf.d/nginx.conf:/etc/nginx/conf.d/default.conf
    hostname: ${HOSTNAME}
    restart: always
    network_mode: host
EOF

cat > /opt/asterisk/bin/asterisk << 'EOF'
#!/bin/bash
if [[ -e /var/run/asterisk/asterisk.ctl ]]; then
  exec /usr/sbin/asterisk "$@"
fi
exec sudo /opt/asterisk/bin/polaris-asterisk "$@"
EOF

cat > /opt/asterisk/bin/polaris-asterisk << 'EOF'
#!/bin/bash

opts=""
[[ -t 0 ]] && opts="-i $opts"
[[ -t 1 ]] && opts="-t $opts"

exec docker exec $opts asterisk asterisk "$@"
EOF

cat > /opt/asterisk/bin/polaris-shell << 'EOF'
#!/bin/bash
exec docker exec -it asterisk su asterisk
EOF


cat > /opt/asterisk/etc/nginx/conf.d/nginx.conf << EOF
server {
    listen       80  default_server;
    server_name  *.asterisk.com;

    listen 8089 ssl;

    ssl_certificate /etc/asterisk/keys/asterisk.pem;
    ssl_certificate_key /etc/asterisk/keys/asterisk.key;

    ssl_session_timeout 5m;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers  HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers on;

    #charset koi8-r;
    access_log  /var/log/nginx/access.log  main;

    location /ws {

        # prevents 502 bad gateway error
        proxy_buffers 8 32k;
        proxy_buffer_size 64k;

        # redirect all HTTP traffic to localhost:8088;
        proxy_pass http://0.0.0.0:8088/ws;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        # enables WS support
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_read_timeout 999999999;

    }

    location /recordings/ {
        root   /dev/shm;

        index index.html index.htm;
        dav_methods PUT DELETE MKCOL COPY MOVE;
        create_full_put_path  off;
        dav_access  group:rw  all:r;

        limit_except GET {
                allow 10.0.0.0/8;
                deny  all;
        }
    }

}
EOF

cat > /usr/local/sbin/core-dumper << 'EOF'
#!/bin/sh

[[ -z "$1" ]] && {
  echo "usage: $0 CORE"
  exit 1
}

core=$(basename $1)

docker_image_ls=$(docker image ls | grep asterisk-docker)
docker_image_repos=$(echo $docker_image_ls | awk '{print $1}')
docker_image_tag=$(echo $docker_image_ls | awk '{print $2}')

[[ -z "$docker_image_repos" || -z "$docker_image_tag" ]] && {
  echo "Unable to determinate docker image to use"
  exit 1
}

docker run \
  --rm \
  --name asterisk-coredumper \
  --volume /var/log/asterisk/core:/tmp \
  --entrypoint /bin/bash \
  "${docker_image_repos}:${docker_image_tag}" \
  /var/lib/asterisk/scripts/ast_coredumper "/tmp/$core"
EOF

cat > /usr/local/sbin/asterisk << 'EOF'
#!/bin/bash
if [[ -e /var/run/asterisk/asterisk.ctl ]]; then
  exec /usr/sbin/asterisk "$@"
fi
exec sudo /opt/asterisk/bin/polaris-asterisk "$@"
EOF

chmod +x /usr/local/sbin/asterisk
chmod +x /opt/asterisk/bin/polaris-shell
chmod +x /opt/asterisk/bin/asterisk
chmod +x /opt/asterisk/bin/polaris-asterisk

chown -R asterisk:asterisk /var/log/asterisk
chown -R asterisk:asterisk /var/log/nginx

# Start Asterisk
cd /opt/asterisk
docker-compose up -d --force-recreate
