#!/bin/bash

stderr_log="${AST_STDERR_LOG:-/tmp/asterisk-stderr.log}"
stdout_log="${AST_STDERR_LOG:-/tmp/asterisk-stdout.log}"

umask 022

_sig() {
  echo "$(date +'%Y/%m/%d %I:%M:%S') entry signal received; exiting..."
  test -r /var/run/asterisk/asterisk.pid \
    && pid=$(cat /var/run/asterisk/asterisk.pid) \
    && echo "$(date +'%Y/%m/%d %I:%M:%S') entry killing asterisk pid=$pid" \
    && kill -TERM "$pid"
  exit 1
}
trap _sig INT
trap _sig TERM

echo "$(date +'%Y/%m/%d %I:%M:%S') entry asterisk version=$ASTERISK_VERSION tag=$TAG pwd=$PWD" | tee -a "$stderr_log"
/sbin/asterisk -fg -vvvvvvvvvvvvvvvvv 1>> "$stdout_log" 2>> "$stderr_log" &
pid=$!
renice -n -15 -p "$pid"
wait "$pid"
echo "$(date +'%Y/%m/%d %I:%M:%S') entry asterisk rc=$?" | tee -a "$stderr_log"

ls core.* &>/dev/null && {
  dump_owner="${AST_DUMP_USER:-asterisk}:${AST_DUMP_GROUP:-asterisk}"
  dump_util="${AST_DUMP_UTIL:-/var/lib/asterisk/scripts/ast_coredumper}"
  dump_base="${AST_DUMP_BASE:-/var/log/asterisk/core}"

  mkdir -p "$dump_base" && chown "$dump_owner" "$dump_base" && {
    for soft_core in core.*; do
      hard_core="${dump_base}/asterisk-core-$(date +'%Y%m%d-%H%M%S')"
      echo "$(date +'%Y/%m/%d %I:%M:%S') entry core-dump from=$soft_core to=$hard_core" | tee -a "$stderr_log"
      mv "$soft_core" "$hard_core" 2>&1 | tee -a "$stderr_log"
      [[ -n "$AST_DUMP_EXEC" && -x "$dump_util" ]] && {
        "$dump_util" "$hard_core" 2>&1 | tee -a "$stderr_log" \
          && chown "$dump_owner" "$hard_core"-* 2>&1 | tee -a "$stderr_log"
      }
    done
  }
}
