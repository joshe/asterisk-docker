PS1='\u@\h:\w\$ '
export PS1

PATH=/opt/asterisk/bin:$PATH
export PATH

set -o vi
alias l='/bin/ls -Al'
