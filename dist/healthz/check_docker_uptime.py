#!/usr/bin/env python

from   datetime   import datetime
import docker
import getopt
from   subprocess import Popen, PIPE
import sys

import argparse


#################################
# App
###

class Nag: OK=0;Warn=1;Critical=2;Unknown=3 # Let's play golf.

class App:
  def usage(self, text=None):
    return (Nag.Unknown, ('' if text is None else text+ '\n\n')+
'''

usage: {exe} [options] --name|-n NAME
options:
  --name,      -n  Specifies the container to check (required).
  --min-alive, -a  Specifies the minium alive time delta in seconds.
                   Currently set to {min_alive}.
  --sudo,      -S  Use sudo to execute docker.

extras:
  --help, -?  You are here.

'''.format(exe=sys.argv[0].split('/')[-1], min_alive=self.min_alive).strip())

  def __init__(self):
    self.help      = False
    self.name      = None
    self.sudo      = False
    self.min_alive = 300

  def main(self):
    try:
      opts, args = getopt.getopt(sys.argv[1:], "a:n:S?", ["name=", "min-alive=", "sudo", "help"])
    except getopt.GetoptError as err:
      return self.usage(str(err))

    for opt, val in opts:
      if   opt in ("-?", "--help"):
        self.help = True
      elif opt in ("-a", "--min-alive"):
        self.min_alive = int(val)
      elif opt in ("-n", "--name"):
        self.name = val
      elif opt in ("-S", "--sudo"):
        self.sudo = True
      else:
        assert False, "API error: unplanned option: %s (%s)" % (opt, val)

    if self.help:
      return self.usage()

    if not self.name:
      return self.usage("No container name defined.  Please use '--name' or '-n'.")

    return self.check()

  def check(self):
    client = docker.APIClient()
    struct = client.inspect_container(self.name)
    return self.validate(struct['State']['StartedAt'].split('.')[0])

  def validate(self, started_at):
    # example: 2018-03-06T18:57:02
    nag      = Nag.OK;
    status   = 'Running'
    now      = datetime.utcnow()
    start_dt = datetime.strptime(started_at, '%Y-%m-%dT%H:%M:%S')

    delta = int((now - start_dt).total_seconds())
    if   delta < 0:
      nag = Nag.Critical
      status = 'Future?'
    elif delta < self.min_alive:
      nag = Nag.Critical
      status = 'Restarted'

    return (nag, '{name} {status} (up {delta}s since {started_at})'.format(name=self.name, status=status, delta=delta, started_at=started_at))


#################################
# main
###

def main():
  (app, main) = App().main()
  print(main)
  sys.exit(app)

if __name__ == "__main__":
  main()
