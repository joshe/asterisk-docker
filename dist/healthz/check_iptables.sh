#!/bin/sh

IPT='/sbin/iptables'
GREP='/bin/grep'
AWK='/bin/awk'
EXPR='/usr/bin/expr'
WC='/usr/bin/wc'

STAT=0
OUTPUT=''
CHAINS=`$IPT -nvL | $GREP 'Chain' | $AWK '{ print $2 }'`
declare -a F2BCHAINS
F2BCHAINS=("f2b-blacklist" "f2b-sshd")
declare -i REQD_NO_OF_F2BCHAINS
REQD_NO_OF_F2BCHAINS=2
declare -i NO_OF_F2BCHAINS
NO_OF_F2BCHAINS=0


containsElement () {
  local e
  for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
  return 1
}

for CHAIN in $CHAINS ; do
	if [ "$CHAIN" != 'FORWARD' ] && [ "$CHAIN" != 'OUTPUT' ] && [ `$EXPR substr $CHAIN 1 4` != "LOG_" ] && [[ ! "$CHAIN" =~ 'DOCKER' ]]; then
		CNT=`expr $($IPT -S $CHAIN | $WC -l) '-' 1`
		if [ $CNT -eq 0 ] ; then
			if  `containsElement "$CHAIN" "${F2BCHAINS[@]}"` ; then
				OUTPUT="${OUTPUT}WARNING $CHAIN $CNT rules"
			else
				OUTPUT="${OUTPUT}ERROR $CHAIN $CNT rules!"
				STAT=2
			fi
		fi
		if  `containsElement "$CHAIN" "${F2BCHAINS[@]}"` ; then
			NO_OF_F2BCHAINS=$((NO_OF_F2BCHAINS + 1))
		fi
	fi
done

if [ "$NO_OF_F2BCHAINS" -lt "$REQD_NO_OF_F2BCHAINS" ] ; then
	OUTPUT="${OUTPUT}ERROR Fail2Ban Chains Missing. Is it running?"
	STAT=2
fi

if [ "$STAT" -eq "0" ] ; then
	OUTPUT="EVERYTHINGS GOOD"
fi

echo $OUTPUT

exit $STAT
