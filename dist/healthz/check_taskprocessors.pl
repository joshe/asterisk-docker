#!/usr/bin/env perl

use strict;
use warnings;
use 5.12.0;

use Getopt::Long;

# copied from nagios
my %G_ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3,'DEPENDENT'=>4);


main();


sub main {
  my $opts = { };
  GetOptions($opts,
    'help'
  ) or usage();

  usage() if $opts->{help};

  my $state;
  my $stats = eval {core_show_taskprocessors();};
  if(defined $stats) {
    my $queues = {high => [ ], low => [ ]};
    for my $stat (@$stats) {
      if($stat->{in_queue} > $stat->{high_water}) {
        push @{$queues->{high}}, $stat;
      }
      elsif($stat->{in_queue} > $stat->{low_water}) {
        push @{$queues->{low}}, $stat;
      }
    }

    # check for low water mark errors
    if(@{$queues->{low}} > 0)  {
      $state = 'WARNING';
      notify_queue($state, 'low', $queues->{low});
    }

    # check for high water mark errors
    if(@{$queues->{high}} > 0) {
      $state = 'CRITICAL';
      notify_queue($state, 'high', $queues->{high});
    }

    if(not defined $state) {
      # verify stats were even returned
      if(@$stats > 0) {
        $state = 'OK';
        print "$state: task processors normal\n";
      }
      else {
        $state = 'UNKNOWN';
        print "$state: stats were not returned\n";
      }
    }
  }
  else {
    (my $err = $@) =~ s/\s+at\s+.*//;
    $state = 'UNKNOWN';
    print "$state: $err";
  }

  exit($G_ERRORS{$state});
}

sub usage { die
  "usage: $0 [-help]\n",
  "options:\n",
  "  -help: you are here\n",
}

sub notify_queue {
  my ($state, $type, $queues) = @_;
  print "$state: ";
  for my $queue (@$queues) {
    my $name = $queue->{processor};
    $name =~ s/^[^:]+://;
    print "($name/$queue->{in_queue})";
  }
  print "\n";
}

sub core_show_taskprocessors {
  # stop open() from printing to stderr
  no warnings qw(exec);

  open my $fh, '-|', qw(asterisk -rx), 'core show taskprocessors'
    or die "asterisk: open: $!";

  my @lines;
  while(my $line = <$fh>) {
    next
      unless $line =~ /^
            (?<processor>\S+)
        \s+ (?<processed>\d+)
        \s+ (?<in_queue>\d+)
        \s+ (?<max_depth>\d+)
        \s+ (?<low_water>\d+)
        \s+ (?<high_water>\d+)
      $/x;
    push @lines, {%+};
  }

  close $fh
    or die "asterisk: close: $!";

  return wantarray ? @lines : \@lines;
}
