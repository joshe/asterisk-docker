#!/bin/sh

# Check asterisk channels for length > 9:59:59 plugin for Nagios.
# Last Modified: 2016-02-15

PROGPATH=`dirname $0`
REVISION=`echo '$Revision: 1.1 $' | sed -e 's/[^0-9.]//g'`
DATETIME=`date "+%Y-%m-%d %H:%M:%S"`
TAB="	"

#. $PROGPATH/utils.sh

print_usage() {
    echo "
Usage: check_asterisk_concurrent_channels [-w <max_channels>] [-c <max_channels>]
Usage: check_asterisk_concurrent_channels --help | -h

Description:

This plugin checks an asterisk server for concurrent Channels.

Tested to work on Linux.

The following arguments are accepted:

  -w              (Optional) Generate a warning message if the defined number
                  channels is exceeded.

  -c              (Optional) Generate a critical message if the defined number
                  channels is exceeded.

  --help | -h     Print this help and exit.

Examples:

List all peers and channel counts, all will be marked as critical with no thresholds set.

  check_asterisk_locked_channels

Check peers and channel counts.  Issue a warning if a peer has 1 or more active channels
and a critical if a peer has 3 or more active channels.

  check_asterisk_locked_channels -w 1 -c 3
"
}

print_help() {
    print_usage
    echo "Check asterisk concurrent channels plugin for Nagios."
    echo ""
}

# Sets the exit status for the plugin.  This is done in such a way that the
# status can only go in one direction: OK -> WARNING -> CRITICAL.
set_exit_status() {
	new_status=$1
	# Nothing needs to be done if the state is already critical, so exclude
	# that case.
	# echo "Changing exit status to $new_status from exit status $exitstatus"
	case "$exitstatus"
	in
		WARNING)
			# Only upgrade from warning to critical.
			if [ "$new_status" = "$STATE_CRITICAL" ]; then
				exitstatus=$new_status;
			fi
		;;
		OK)
			# Always update state if current state is OK.
			exitstatus=$new_status;
		;;
	esac
}

# Ensures that a call to the Asterisk process returns successfully.  Exits
# critical if not.
check_asterisk_result() {
	if [ "$1" != "0" ]; then
		exit $STATE_CRITICAL
	fi
}

# Defaults.

STATE_OK=OK
STATE_WARNING=WARNING
STATE_CRITICAL=CRITICAL

exitstatus=$STATE_OK
openchan_warning=-1
openchan_critical=-1

# Grab the command line arguments.
while test -n "$1"; do
    case "$1" in
        --help)
            print_help
            exit $STATE_OK
            ;;
        -h)
            print_help
            exit $STATE_OK
            ;;
        -w)
            openchan_warning=$2
            shift
            ;;
        -c)
            openchan_critical=$2
            shift
            ;;
        -x)
            exitstatus=$2
            shift
            ;;
        --exitstatus)
            exitstatus=$2
            shift
            ;;
        *)
            echo "Unknown argument: $1"
            print_usage
            exit $STATE_UNKNOWN
            ;;
    esac
    shift
done

# Sanity checking for arguments.
if [ "$openchan_warning" != "-1" ] && ([ ! "$openchan_warning" ] || [ `echo "$openchan_warning" | grep [^0-9]` ]); then
	echo "Number of channels warning value must be a number."
	exit $STATE_UNKNOWN
fi

if [ "$openchan_critical" != "-1" ] && ([ ! "$openchan_critical" ] || [ `echo "$openchan_critical" | grep [^0-9]` ]); then
	echo "Number of channels critical value must be a number."
	exit $STATE_UNKNOWN
fi

if [ "$openchan_warning" != "-1" ] && [ "$openchan_critical" != "-1" ] && [ "$openchan_warning" -ge "$openchan_critical" ]; then
	echo "Critical channels must be greater than warning channels."
	exit $STATE_UNKNOWN
fi


# Fetch the data from asterisk.
#command_output=`asterisk -rx "core show channels concise" | grep '!' | grep -v 'Privilege escalation' | sort -r -g -t '!' -k 12 | cut -d '!' -f1,12`
command_output=`asterisk -rx 'core show channels concise'|awk -F '-0' '$1 !~ /^SIP\/FC/ {count[$1]++}END{for(j in count) print ""count[j]"!"j}'`

open_channel_output=
channel_over_warning=0
channel_over_critical=0

# Figure out if we have any stuck channels
for line in $command_output; do
	IFS='!' read -a array_out <<< "$line"
	channel_count="${array_out[0]}"
	channel_name="${array_out[1]}"
	#echo "Peer $channel_name has $channel_count open channels"
	if [ "$channel_count" -ge "$openchan_warning" ] && [ "$channel_count" -lt "$openchan_critical" ]; then
		#echo "warning!!"
		channel_over_warning=$(($channel_over_warning+1))
		warning_channel_output="[$channel_name,$channel_count] $warning_channel_output"
        elif [ "$channel_count" -ge "$openchan_critical" ]; then
		#echo "critical!!"
                channel_over_critical=$(($channel_over_critical+1))
                critical_channel_output="[$channel_name,$channel_count] $critical_channel_output"
        fi
done

#echo "Warning: $channel_over_warning"
#echo "Critical: $channel_over_critical"

# Test for warning/critical channels.
if [ "$channel_over_critical" -gt "0" ]; then
	set_exit_status $STATE_CRITICAL
elif [ "$channel_over_warning" -gt "0" ] && [ "$channel_over_critical" = "0" ]; then
	set_exit_status $STATE_WARNING
elif [ "$channel_over_warning" = "0" ] && [ "$channel_over_critical" = "0" ];then
	# echo "Setting exit status to $STATE_OK"
        set_exit_status $STATE_OK
fi

case $exitstatus
in
	CRITICAL)
		exit_code=2;
		exit_message="CRITICAL";
	;;
	WARNING)
		exit_code=1;
		exit_message="WARNING";
	;;
	OK)
		exit_code=0;
		exit_message="OK";
	;;
	*)
		exit_code=3;
		exit $STATE_UNKNOWN;
		
	;;
esac

echo "${exit_message}: Concurrent WARNING: $warning_channel_output; Concurrent CRITICAL: $critical_channel_output |${DATETIME}";

exit $exit_code

