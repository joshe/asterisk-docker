#!/usr/bin/env python
import argparse
import ari
import sys
import time
import yaml
import docker

# Nagios exit codes
OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

# Number of seconds to wait after writing call file before checking channels
CHECK_DELAY = 2
MAX_DELAY = 8

# Parse cli args
parser = argparse.ArgumentParser(description="ARI Stasis Health Check")
parser.add_argument("--config", help="Path to config file", default="config.yml")
args = parser.parse_args()

# Try loading config file
config = None
try:
    with open(args.config) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
except Exception as e:
    print("Failed to load config file: {}".format(e))
    sys.exit(UNKNOWN)
stasis_app = config['ari']['apps']['healthz']

# Establish ARI connection
client = None
try:
    client = ari.connect(
        config["ari"]["host"], config["ari"]["user"], config["ari"]["password"])
except Exception as e:
    print("Failed to connect to ari: {}".format(e))
    sys.exit(UNKNOWN)

# Originate call to stasis health check
channel = client.channels.originate(
    endpoint='Local/{}@healthz'.format(stasis_app),
    app=stasis_app, timeout=10
)

# We needs to wait for the channel to be created and put into stasis before we
# try to get the channel vars
current_delay = 0
healthz = None
while current_delay <= MAX_DELAY:
    time.sleep(CHECK_DELAY)
    # Try getting the healthz var from the channel
    try:
        healthz = channel.getChannelVar(variable='healthz').get('value')
        break
    except:
        current_delay += CHECK_DELAY
        continue

# Try to hangup the channel since we are done with it, ok if this fails
# timeout should kill this eventually
try:
    channel.hangup()
except:
    pass

# Check if our healthz variable got set correctly
if healthz == 'ok':
    print('Stasis set channel var successfully')
    sys.exit(OK)

# If we got here we didn't find the channel var
# Check if we should attempt a container restart
msg = 'Unable to find channel var from stasis'
if config.get('healthz', {}).get('restart', False):
    client = docker.from_env()
    client.restart(config['healthz']['container'], 0)
    msg += ', attempted container restart'
	
print(msg)
sys.exit(CRITICAL)

