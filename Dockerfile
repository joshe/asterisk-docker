FROM alpine:3.12 as go-builder

RUN apk --no-cache update \
 && apk --no-cache upgrade \
 && apk --no-cache add git go musl-dev

FROM centos:7

ENV ENV                 polaris
ENV MARIADBODBC_VERSION 3.1.7
ENV UNIXODBC_VERSION    2.3.7
ENV LIBSRTP_VERSION     2.3.0
ENV CMAKE_VERSION       3.16.2
ENV ASTERISK_VERSION    17.7.0
ENV G72X_VERSION        1.4.3

########################################################################################################################
# bootstrap build environment

RUN yum install -y epel-release \
 && yum update -y \
 && yum install -y gcc gcc-c++ make autoconf automake libtool patch kernel-devel \
    libuuid libuuid-devel libedit libedit-devel libxml2 libxml2-devel libpcap-devel libcurl-devel libjpeg-devel \
    ncurses ncurses-devel openssl openssl-devel pcre-devel sqlite sqlite-devel spandsp spandsp-devel gmime gmime-devel \
    gsm gsm-devel ilbc ilbc-devel libvorbis libvorbis-devel vorbis-tools opus opus-devel speex speex-devel speex-tools \
    lua lua-devel net-snmp net-snmp-devel bzip2 file git iproute subversion wget which xmlstarlet lua-sec lua-socket \
    lua-json

RUN mkdir -p /usr/src \
 && echo "/usr/local/lib" > /etc/ld.so.conf.d/local.conf \
 && ldconfig

########################################################################################################################
# MariaDB ODBC driver
# https://downloads.mariadb.org/connector-odbc/

RUN cd /usr/src \
 && mkdir -p /usr/src/mariadb-connector-odbc-$MARIADBODBC_VERSION-ga-rhel7-x86_64 \
 && cd /usr/src/mariadb-connector-odbc-$MARIADBODBC_VERSION-ga-rhel7-x86_64 \
 && wget http://downloads.mariadb.com/Connectors/odbc/connector-odbc-$MARIADBODBC_VERSION/mariadb-connector-odbc-$MARIADBODBC_VERSION-ga-rhel7-x86_64.tar.gz \
 && tar xzvf mariadb-connector-odbc-$MARIADBODBC_VERSION-ga-rhel7-x86_64.tar.gz

RUN cd /usr/src/mariadb-connector-odbc-$MARIADBODBC_VERSION-ga-rhel7-x86_64 \
 && cp lib64/libmaodbc.so /usr/lib64/libmaodbc-$MARIADBODBC_VERSION \
 && ln -s /usr/lib64/libmaodbc-$MARIADBODBC_VERSION /usr/lib64/libmyodbc5.so

########################################################################################################################
# unixODBC
# http://www.unixodbc.org/download.html

RUN cd /usr/src \
 && wget ftp://ftp.unixodbc.org/pub/unixODBC/unixODBC-$UNIXODBC_VERSION.tar.gz \
 && tar xzvf unixODBC-$UNIXODBC_VERSION.tar.gz

RUN cd /usr/src/unixODBC-$UNIXODBC_VERSION \
 && ./configure --sysconfdir=/etc --libdir=/usr/lib64 --enable-drivers=yes --enable-driverc=yes \
 && make CFLAGS="-g" -j$(nproc) \
 && make install


########################################################################################################################
# libsrtp
# https://github.com/cisco/libsrtp/releases

RUN cd /usr/src \
 && git clone https://github.com/cisco/libsrtp.git

RUN cd /usr/src/libsrtp \
 && git checkout -b 94ac00d5ac6409e3f6409e4a5edfcdbdaa7fdabe \ 
 && ./configure CFLAGS="-g" \
 && make \
 && make shared_library \
 && make install


########################################################################################################################
# cmake
# https://cmake.org/download/

RUN cd /usr/src \
 && wget https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-Linux-x86_64.tar.gz \
 && tar -C /usr/local -xzvf cmake-$CMAKE_VERSION-Linux-x86_64.tar.gz \
 && ln -s /usr/local/cmake-$CMAKE_VERSION-Linux-x86_64/bin/cmake /usr/bin/cmake

########################################################################################################################
# bcg729
# http://github.com/BelledonneCommunications/bcg729.git

RUN cd /usr/src \
 && git clone git://github.com/BelledonneCommunications/bcg729.git

RUN cd /usr/src/bcg729 \
 && cmake . -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_PREFIX_PATH=/usr \
 && make \
 && make install


########################################################################################################################
# asterisk

RUN cd /usr/src \
 && wget https://github.com/asterisk/asterisk/archive/$ASTERISK_VERSION.tar.gz -O asterisk-$ASTERISK_VERSION.tar.gz \
 && tar xzf asterisk-$ASTERISK_VERSION.tar.gz

COPY dist/patch.d /usr/src/asterisk-$ASTERISK_VERSION/patch.d

RUN cd /usr/src/asterisk-$ASTERISK_VERSION \
 && sed -i 's/all: asterisk/all: asterisk\nAST_LIBS+=-lbcg729/' main/Makefile \
 && sed -i 's/#define AST_TASKPROCESSOR_HIGH_WATER_LEVEL .*/#define AST_TASKPROCESSOR_HIGH_WATER_LEVEL 3000/' include/asterisk/taskprocessor.h \
 && sed -i 's/${HOSTNAME}/'"${ENV}"'/' build_tools/make_build_h \
 && for patch in patch.d/*.patch; do \
      echo "[PATCH] Applying $patch..."; \
      patch -p1 < $patch || exit 1; \
    done

RUN cd /usr/src/asterisk-$ASTERISK_VERSION \
 && ./configure --with-jansson-bundled \
 && contrib/scripts/get_mp3_source.sh \
 && make menuselect.makeopts \
 && menuselect/menuselect --enable  BETTER_BACKTRACES            menuselect.makeopts \
 && menuselect/menuselect --disable BUILD_NATIVE                 menuselect.makeopts \
 && menuselect/menuselect --disable COMPILE_DOUBLE               menuselect.makeopts \
 && menuselect/menuselect --enable  DONT_OPTIMIZE                menuselect.makeopts \
 && menuselect/menuselect --enable  ODBC_STORAGE                 menuselect.makeopts \
 && menuselect/menuselect --disable app_alarmreceiver            menuselect.makeopts \
 && menuselect/menuselect --enable  app_macro                    menuselect.makeopts \
 && menuselect/menuselect --disable app_minivm                   menuselect.makeopts \
 && menuselect/menuselect --disable app_voicemail                menuselect.makeopts \
 && menuselect/menuselect --disable chan_phone                   menuselect.makeopts \
 && menuselect/menuselect --disable chan_iax2                    menuselect.makeopts \
 && menuselect/menuselect --disable chan_mgcp                    menuselect.makeopts \
 && menuselect/menuselect --disable chan_misdn                   menuselect.makeopts \
 && menuselect/menuselect --disable chan_oss                     menuselect.makeopts \
 && menuselect/menuselect --enable  chan_sip                     menuselect.makeopts \
 && menuselect/menuselect --disable chan_skinny                  menuselect.makeopts \
 && menuselect/menuselect --disable chan_unistim                 menuselect.makeopts \
 && menuselect/menuselect --disable chan_vpb                     menuselect.makeopts \
 && menuselect/menuselect --disable chan_motif                   menuselect.makeopts \
 && menuselect/menuselect --enable  codec_opus                   menuselect.makeopts \
 && menuselect/menuselect --enable  codec_silk                   menuselect.makeopts \
 && menuselect/menuselect --enable  codec_siren7                 menuselect.makeopts \
 && menuselect/menuselect --enable  codec_siren14                menuselect.makeopts \
 && menuselect/menuselect --disable res_odbc                     menuselect.makeopts \
 && menuselect/menuselect --disable res_odbc_transaction         menuselect.makeopts \
 && menuselect/menuselect --disable cdr_csv                      menuselect.makeopts \
 && menuselect/menuselect --enable  cdr_manager                  menuselect.makeopts \
 && menuselect/menuselect --disable cdr_pgsql                    menuselect.makeopts \
 && menuselect/menuselect --disable cdr_radius                   menuselect.makeopts \
 && menuselect/menuselect --disable cdr_sqlite3_custom           menuselect.makeopts \
 && menuselect/menuselect --disable cdr_tds                      menuselect.makeopts \
 && menuselect/menuselect --disable cel_pgsql                    menuselect.makeopts \
 && menuselect/menuselect --disable cel_radius                   menuselect.makeopts \
 && menuselect/menuselect --disable cel_sqlite3_custom           menuselect.makeopts \
 && menuselect/menuselect --disable cel_tds                      menuselect.makeopts \
 && menuselect/menuselect --enable  format_mp3                   menuselect.makeopts \
 && menuselect/menuselect --disable pbx_ael                      menuselect.makeopts \
 && menuselect/menuselect --disable app_followme                 menuselect.makeopts \
 && menuselect/menuselect --enable  pbx_lua                      menuselect.makeopts \
 && menuselect/menuselect --disable res_ari_mailboxes            menuselect.makeopts \
 && menuselect/menuselect --enable  res_hep                      menuselect.makeopts \
 && menuselect/menuselect --enable  res_hep_pjsip                menuselect.makeopts \
 && menuselect/menuselect --enable  res_hep_rtcp                 menuselect.makeopts \
 && menuselect/menuselect --disable res_mwi_external             menuselect.makeopts \
 && menuselect/menuselect --disable res_phoneprov                menuselect.makeopts \
 && menuselect/menuselect --disable res_pjsip_phoneprov_provider menuselect.makeopts \
 && menuselect/menuselect --enable  res_snmp                     menuselect.makeopts \
 && menuselect/menuselect --enable  CORE-SOUNDS-EN-WAV           menuselect.makeopts \
 && menuselect/menuselect --enable  CORE-SOUNDS-EN-ULAW          menuselect.makeopts \
 && menuselect/menuselect --enable  CORE-SOUNDS-EN-G729          menuselect.makeopts \
 && menuselect/menuselect --enable  CORE-SOUNDS-EN-G722          menuselect.makeopts \
 && menuselect/menuselect --enable  CORE-SOUNDS-EN-SLN16         menuselect.makeopts \
 && make -j$(nproc) \
 && make install

########################################################################################################################
# g729 codec
# http://asterisk.hosting.lv/

RUN cd /usr/src \
 && wget http://asterisk.hosting.lv/src/asterisk-g72x-$G72X_VERSION.tar.bz2 \
 && tar xjvf asterisk-g72x-$G72X_VERSION.tar.bz2

RUN cd /usr/src/asterisk-g72x-$G72X_VERSION \
 && ./autogen.sh \
 && ./configure --with-bcg729 --with-asterisk160 --with-asterisk-includes=/usr/src/asterisk-$ASTERISK_VERSION/include \
 && make \
 && make install


########################################################################################################################
# sipgrep

RUN cd /usr/src \
 && git clone https://github.com/sipcapture/sipgrep.git

RUN cd /usr/src/sipgrep \
 && ./build.sh \
 && ./configure \
 && sed -i '/NOT PARSED/ d' src/sipgrep.c \
 && make \
 && make install \
 && mv /usr/local/bin/sipgrep /usr/local/bin/sipgrep.bin \
 && chmod u+s /usr/local/bin/sipgrep.bin

########################################################################################################################
# operational dependencies

RUN yum -y install curl gdb jq python-pip python-devel mtr tcpdump

########################################################################################################################
# post-installation

# asterisk user
RUN groupadd -g 1001 asterisk \
 && useradd -u 1001 -g 1001 -s /bin/bash -md /home/asterisk asterisk

COPY dist/asterisk/.bashrc /home/asterisk/.bashrc

RUN chown -R asterisk.asterisk /home/asterisk    \
 && chown -R asterisk.asterisk /usr/lib/asterisk \
 && chown -R asterisk.asterisk /var/lib/asterisk \
 && chown -R asterisk.asterisk /var/log/asterisk \
 && chown -R asterisk.asterisk /var/run/asterisk \
 && chown -R asterisk.asterisk /var/spool/asterisk

# setuid programs
RUN chmod u+s /usr/sbin/mtr \
 && chmod u+s /usr/sbin/tcpdump

# timezone
RUN ln -fs /usr/share/zoneinfo/America/Denver /etc/localtime

# clean-up
RUN rm -rf /home/asterisk/.bash_* /home/asterisk/.odbc.ini \
 && rm -rf /tmp/* /tmp/.*-unix

# distributable files
COPY dist/sipgrep /usr/local/bin/
COPY dist/files/one_tenth.wav /var/lib/asterisk/sounds/
COPY dist/files/hangup.sln /var/lib/asterisk/sounds/
COPY dist/unixODBC/odbcinst.ini /etc/
COPY dist/root/.bashrc /root/.bashrc
COPY dist/entrypoint /

########################################################################################################################
# entry

WORKDIR /home/asterisk

ENTRYPOINT ["/entrypoint"]

ARG TAG
ENV TAG $TAG
